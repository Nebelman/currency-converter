import javax.swing.*;
import java.math.BigDecimal;

public class Ep01Function {

    public boolean check(String input) {
        try {
            double x = Double.parseDouble(input);
            if (x >= 0 || x < 0);
                return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public void rubleToDol(double Minput) {
        double Dol = Minput * 76.00;
        JOptionPane.showMessageDialog(null, "В "+ Minput +" долларах " + Dol + " рублей");
    }

    public void rubleToEUR(double Minput) {
        double EUR = Minput * 91.00;
        JOptionPane.showMessageDialog(null, "В "+ Minput +" евро " + EUR + " рублей");
    }
}

import javax.swing.*;

public class Ep01Runner {
    public static void main(String[] args) {
        Ep01Function f = new Ep01Function();
        Object[] option1 = {"Доллар", "Евро", "Выйти"};
        Object[] option2 = {"Повторить", "Выйти"};

        while (true) {
            String input = JOptionPane.showInputDialog(null, "Введите сумму: ");
            System.out.println(input);

            if (f.check(input) == true) {
                double Minput = Double.parseDouble(input);

                int choice1 = JOptionPane.showOptionDialog(null, "Выберите валюту, которую вы хотите конвертировать",
                        "Валютный конвертер", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                        option1,option1[2]);



                if (choice1 == 0){
                    f.rubleToDol(Minput);

                    int choice2 = JOptionPane.showOptionDialog(null, "Повторить попытку? ",
                            "Валютный конвертер",
                            JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, option2,option2[1]);

                    if (choice2 == 0){

                    }else { break; }

                }else if(choice1 == 1){
                    f.rubleToEUR(Minput);

                    int choice2 = JOptionPane.showOptionDialog(null, "Повторить попытку? ",
                            "Валютный конвертер",
                            JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, option2,option2[1]);

                    if (choice2 == 0){

                    }else { break; }

                }else {break;}
            }
            else {
                JOptionPane.showMessageDialog(null, "Неправильный формат, повторите попытку");

                int choice2 = JOptionPane.showOptionDialog(null, "Повторить попытку? ",
                        "Валютный конвертер",
                        JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, option2,option2[1]);

                if (choice2 == 0){

                }else { break; }


            }
        }
    }
}
